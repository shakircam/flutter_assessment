import 'package:flutter_assessment/app/core/services/service_locator.dart';
import 'package:flutter_assessment/app/data/local/repo/github_repo_local_source.dart';
import 'package:flutter_assessment/app/data/local/repo/model/repository_entity.dart';
import 'package:flutter_assessment/app/data/remote/github%20repo/github_repo_remote_source.dart';
import 'package:flutter_assessment/app/data/remote/github%20repo/model/github_repository.dart';
import 'package:flutter_assessment/app/data/remote/github%20repo/model/github_repository_params.dart';
import 'package:flutter_assessment/app/data/remote/github%20repo/model/github_repository_response.dart';
import 'github_repository.dart';

class GitHubRepositoryImpl implements GitHubRepository {
  final GitHubRepositoryRemoteSource remoteSource;
  final GithubRepoLocalSource localSource;

  GitHubRepositoryImpl({
    required this.remoteSource,
    required this.localSource,
  });

  @override
  Future<List<RepositoryEntity>> getRepositoryList(
      GitHubRepositoryParams params) async {
    try {
      final localData = await localSource.getRepositoryList(params);
      if (localData.isNotEmpty) {
        return localData;
      }

      final remoteData = await remoteSource.getRepositoryList(params);
      await _insertToLocal(remoteData);
      return _getFromLocal(params);
    } catch (error) {
      rethrow;
    }
  }

  List<RepositoryEntity> convertGitHubRepositoriesToEntities(
    List<RemoteGitHubRepository>? repos,
  ) {
    return repos?.map((repo) {
          return RepositoryEntity(
            id: repo.id,
            name: repo.name,
            fullName: repo.fullName,
            description: repo.description,
            login: repo.owner.login,
            imageUrl: repo.owner.avatar,
            updatedAt: repo.updatedAt,
            language: repo.language,
          );
        }).toList() ??
        [];
  }

  Future<void> _insertToLocal(RemoteGitHubRepositoryResponse value) async {
    await localSource
        .insertRepositoryList(convertGitHubRepositoriesToEntities(value.items));
  }

  Future<List<RepositoryEntity>> _getFromLocal(
      GitHubRepositoryParams params) async {
    return await localSource.getRepositoryList(params);
  }
}
